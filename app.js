const express = require("express");
const path = require("path");
const { Pool } = require("pg");

//  reation du serveur
const app = express()

//configuration du serveur
app.set("view engine", "ejs");
app.set('views', './views');
app.use(express.static('public'));

//connexion a la db
const pool = new Pool({
  user: "ordipret",
  host: "localhost",
  database: "dbquizz",
  password: "untruc",
  port: 5432
});
pool.connect();
console.log("Connexion réussie à la base de données");
global.pool = pool;

//imports modules
var all_themes = require('./models/themes.js')
var questions_for_quiz = require('./models/questions.js')
var answers_for_question = require('./models/answers.js')
var all_quizz = require('./models/datas.js')

//declaration de port et d'export
module.exports = app;
const port = 3000

// index page 
app.get('/', function (req, res) {
  res.render('index.ejs', { title: 'Hey', message: 'Hello there!'});
});

app.get('/quizz', async function (req, res) {
    const responses = await all_themes();
    res.render('quizz.ejs', { title: 'Hey', themes: responses});
    });


app.get('/quizz/:id', async function (req, res) {
  const questions = await questions_for_quiz(req.params['id']);
  const quizz = []

  for (let question of questions) {
    let question_data = {}
    let answers       = await answers_for_question(question['id'])

    question_data['id'] = question['id']
    question_data['question'] = question['query']
    question_data['answers'] = []

    for (let answer of answers) {
      question_data['answers'].push(answer['texte'])
    }
    quizz.push(question_data)
  }
    res.render('questions.ejs', { title: 'Hey', questions: questions, quizz: quizz});
    });

//voir le resultat
app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})